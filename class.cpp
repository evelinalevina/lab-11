#include "class.hpp"
#include <iostream>

namespace el
{
	Signal::Signal(std::string name, int adress)
	{
		m_name = name;
		m_adress = adress;
	}
	Signal::~Signal() {};

	void Signal::Poll()
	{
		if (m_adress == 0)
		{
			a = 0;
			b = 1;
		}
		else if (m_adress == 1)
		{
			a = 1;
			b = 0;
		}

	}
	void Signal::Print()
	{
		std::cout << "Adress: " << m_adress << std::endl;
		std::cout << "Name: " << m_name << std::endl;
		std::cout << "1: " << a << std::endl;
		std::cout << "2: " << b << std::endl;
	}

   

}
