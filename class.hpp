#pragma once
#include <iostream>

namespace el
{
    
	class Signal
	{
		int m_adress;
		std::string m_name;
		bool a, b;
	public:
		Signal(std::string name, int adress);
		~Signal();
		void Poll();
		void Print();
	};

   
    
}
