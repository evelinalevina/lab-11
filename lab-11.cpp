﻿#include <iostream>
#include "class.hpp"
#include <vector>

using namespace el;

int main()
{
	setlocale(LC_ALL, "Rus");
	std::vector<Signal*> signals;
	Signal* signal = new Signal("Reallab NL16HV", 0);
	signals.push_back(signal);
	signal = new Signal("ЭНМВ-1-24", 1);
	signals.push_back(signal);

	for (const auto& i : signals)
	{
		i->Poll();
		i->Print();
	}
	for (int i = 0; i < signals.size(); i++)
	{
		delete signals[i];
	}


}


